from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import *;
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options   

class Lab7Unittest(TestCase):
    def test_url_exist(self):
        response = Client().get(reverse('story_9:loginView'))
        self.assertEqual(response.status_code, 200)

    def test_url_not_exist(self):
        response = Client().get('/blank')
        self.assertEqual(response.status_code, 404)

    def test_page(self):
        response = Client().get(reverse('story_9:loginView'))
        self.assertTemplateUsed(response, 'login.html')

    def test_url_exist_2(self):
        response = Client().get(reverse('story_9:logoutView'))
        self.assertEqual(response.status_code, 200)

    def test_page_2(self):
        response = Client().get(reverse('story_9:logoutView'))
        self.assertTemplateUsed(response, 'logout.html')

class Lab7FunctionalTest(LiveServerTestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=options)

    def tearDown(self):
        self.browser.close()

    def test_title(self):
        self.browser.get(self.live_server_url)

        self.assertEquals(self.browser.title, 'Story 7 & 8 & 9')

    def test_can_login(self):
        # Create user
        User.objects.create_user(username='najib', email='najib@mail.com', password='test123')

        self.browser.get(self.live_server_url + "/sign_in/login")

        username = self.browser.find_element_by_id('username_login')
        password = self.browser.find_element_by_id('password_login')
        submit = self.browser.find_element_by_id('submit_login')

        username.send_keys('najib')
        password.send_keys('test123')
        submit.click()
        time.sleep(10)

        self.assertIn("Hello najib, welcome to my website.", self.browser.page_source)
        time.sleep(10)



    def test_auth_can_access_logout(self):
        # Create user
        User.objects.create_user(username='najib', email='najib@mail.com', password='test123')

        self.browser.get(self.live_server_url + "/sign_in/logout")
        
        logout = self.browser.find_element_by_id("logout")
        logout.click()
        time.sleep(10)

        self.assertIn("Hello , welcome to my website.", self.browser.page_source)
        time.sleep(10)

    def test_login_with_wrong_username(self):
       self.browser.get(self.live_server_url + "/sign_in/login")



       username = self.browser.find_element_by_id('username_login')
       password = self.browser.find_element_by_id('password_login')
       submit = self.browser.find_element_by_id('submit_login')

       username.send_keys('najib')
       password.send_keys('test123')
       submit.click()
       time.sleep(10)
       self.authpage_login = reverse("story_9:loginView")
       response4 = Client().get(self.authpage_login)



       self.assertTemplateUsed(response4, 'login.html')
       time.sleep(10)
    
    # def test_signUp_success(self):
    #     # self.browser.get(self.live_server_url + "/sign_in/register")
    #     response = self.client.post('/sign_in/register')




