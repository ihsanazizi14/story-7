from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from . import forms

response = {}

# Create your views here.
def register(response):
    if response.method == 'POST':
        form = forms.RegisterForm(response.POST)
        if form.is_valid():
            form.save()
            return redirect("story_9:loginView")
    else:
	    form = forms.RegisterForm()

    return render(response, 'register.html', {"form":form})

def loginView(request):

    user = None
    if request.method == "POST":

        username_login = request.POST['username']
        password_login = request.POST['password']
		
        user = authenticate(request, username=username_login, password=password_login)

        if user is not None:
            login(request, user)
            request.session['username'] = username_login
            response['username'] = request.session['username']


            return redirect('my_app:index')
        else:            
            return redirect("story_9:loginView")
            
    return render(request, 'login.html', response)


def logoutView(request):
    if request.method == "POST":
        if request.POST["logout"] == "Submit":
            request.session.flush()
            logout(request)

        return redirect('my_app:index')	

    return render(request, 'logout.html')