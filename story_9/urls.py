from django.urls import path
from . import views

from django.contrib.auth import authenticate, login, logout

app_name = 'story_9'

urlpatterns = [
    path('login/', views.loginView, name='loginView'),
    path('register/', views.register, name ='register'),
    path('logout/', views.logoutView, name='logoutView'),
    
]