from django.test import TestCase, Client, LiveServerTestCase
from .views import *;
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options   
from django.urls import reverse


class Lab7Unittest(TestCase):
    def test_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_exist(self):
        response = Client().get('/blank')
        self.assertEqual(response.status_code, 404)

    def test_page(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story7.html')

    def test_url_exist_2(self):
        response = Client().get('/ajax/')
        self.assertEqual(response.status_code, 200)

    def test_page_2(self):
        response = Client().get('/ajax/')
        self.assertTemplateUsed(response, 'ajax.html')

class Lab7FunctionalTest(LiveServerTestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=options)

    def tearDown(self):
        self.browser.close()

    def test_title(self):
        self.browser.get(self.live_server_url)

        self.assertEquals(self.browser.title, 'Story 7 & 8 & 9')


    def test_bisa_accordion_activities(self):
        self.browser.get(self.live_server_url)

        activities = self.browser.find_element_by_id("education")
        activities.click()
        self.assertIn( "- Al-Khairaat Islamic Elementary School", self.browser.page_source)

    def test_bisa_accordion_organizations(self):
        self.browser.get(self.live_server_url)

        organizations = self.browser.find_element_by_id("organization")
        organizations.click()
        self.assertIn("- FUKI Fasilkom UI", self.browser.page_source)
    
    def test_bisa_accordion_achievements(self):
        self.browser.get(self.live_server_url)

        achievements = self.browser.find_element_by_id("committe")
        achievements.click()
        self.assertIn("- Compfest XI", self.browser.page_source)

    def test_ajax_can_get_data(self):
        self.browser.get(self.live_server_url + '/ajax')    
        time.sleep(1)
        element = self.browser.find_element_by_id("search")
        element.send_keys("Indonesia")
        submit = self.browser.find_element_by_id("button")
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        assert "Indonesia" in self.browser.page_source


