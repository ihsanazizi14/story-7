from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'story7.html')

def ajax(request) :
    return render(request, 'ajax.html')