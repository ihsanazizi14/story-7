from django.urls import path, include
from . import views

app_name = 'my_app'

urlpatterns = [
    path('', views.index, name='index'),
    # path('', include("django.contrib.auth.urls")),
    path('ajax/', views.ajax, name='ajax'),


]